# mirarticles

Articles app. Test project for m.i.r. Django developer job application.

Deployed at [https://mirarticles-tbitai.rhcloud.com](https://mirarticles-tbitai.rhcloud.com)

## Installing for development

The code was developed in Python 2.7, so first you should set up a Python 2.7 environment (e.g. a virtualenv).

Clone the Git repo and go to the repo root directory.

Install requirements:

```
pip install -r requirements.txt
```

Go to the `wsgi/django_project` directory.

To be able to run Django management commands, you need to set OS environment variables for email settings. `setenv.sh.template` is a template for a Bash script that sets all the necessary variables. Based on the template, write a script called `setenv.sh` that contains your settings, and source it:
 
```
. ./setenv.sh
```

Run migrations:

```
python manage.py migrate
```

Create a superuser:

```
python manage.py createsuperuser
```

Now you're ready to run the app:

```
python manage.py runserver
```
