from django.core.paginator import Paginator
from django.core.urlresolvers import reverse
from django.http import Http404
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView

from articles.models import ContactRequest, Article


class ContactRequestCreate(CreateView):
    model = ContactRequest
    fields = ['email', 'name', 'content']
    success_url = '/'  # Home URL is always `/`, so hardcoding is fine here.


def article_list(request, pagenum='1'):
    pagenum = int(pagenum)

    online_articles = Article.objects.filter(online=True).order_by('-publication_date')
    paginator = Paginator(online_articles, 5)
    articles = paginator.page(pagenum)

    return render(request, 'articles/article_list.html', {'articles': articles})


class ArticleDetail(DetailView):
    model = Article

    def get_context_object_name(self, obj):
        if not obj.online:
            raise Http404()
        return super(ArticleDetail, self).get_context_object_name(obj)

article_detail_view = ArticleDetail.as_view()
