import datetime
from django.contrib import admin

from articles.models import ContactRequest
from articles.models import Article


@admin.register(ContactRequest)
class ContactRequestAdmin(admin.ModelAdmin):
    def has_add_permission(self, request):
        return False

    # def has_change_permission(self, request, obj=None):
    #     return False
    def save_model(self, request, obj, form, change):
        pass
    change_form_template = 'articles/contactrequest_delete_form.html'

    def has_delete_permission(self, request, obj=None):
        return True


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('title',)}

    def get_changeform_initial_data(self, request):
        return {'author': request.user,
                'publication_date': datetime.datetime.now()}
