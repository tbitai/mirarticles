from __future__ import unicode_literals

import datetime
from django.core.mail.message import EmailMessage
from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

from mirarticles.settings import CONTACT_EMAIL_FROM, CONTACT_EMAIL_TO


class ContactRequest(models.Model):
    email = models.EmailField()
    name = models.CharField(max_length=100)
    content = models.TextField()
    date = models.DateField(auto_now_add=True)

    def save(self, *args, **kwargs):
        super(ContactRequest, self).save(*args, **kwargs)
        msg = EmailMessage('mirarticles contact request',
                           self.name + ' <' + self.email + '> wrote:\n\n' +
                           self.content,
                           CONTACT_EMAIL_FROM,
                           [CONTACT_EMAIL_TO],
                           reply_to=[self.email])
        msg.send()

    def __unicode__(self):
        return self.name + ' <' + self.email + '> ' + self.date.strftime('%x') + ' ' \
               + self.content[:20] + ('...' if len(self.content) > 20 else '')


class Article(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    author = models.ForeignKey(User)
    publication_date = models.DateField()
    content = models.TextField()
    online = models.BooleanField(default=False)

    def __unicode__(self):
        return self.author.get_full_name() + ', "' + self.title + '"'

    def get_absolute_url(self):
        return reverse('articles.views.article_detail_view', kwargs={'slug': str(self.slug)})
